package com.zeto.kooteam.controller;

import com.blade.mvc.http.Cookie;
import com.zeto.ZenData;
import com.zeto.ZenResult;
import com.zeto.ZenUserHelper;
import com.zeto.annotation.AccessRole;
import com.zeto.domain.ZenRole;
import com.zeto.domain.ZenUser;

import java.util.ArrayList;
import java.util.List;

@AccessRole(ZenRole.NORMAL)
public class Home {
    // 退出系统
    public ZenResult quit() {
        List<Cookie> newCookie = new ArrayList<>();
        Cookie cookie = new Cookie();
        cookie.name("uid");
        cookie.value("");
        cookie.maxAge(-1);
        newCookie.add(cookie);
        return ZenResult.success().setCookies(newCookie);
    }

    public ZenResult nick(ZenData data) {
        String uid = data.get("uid");
        ZenUser user = ZenUserHelper.i().get(uid);
        if (user == null) {
            return ZenResult.success().put("nick", "");
        }
        return ZenResult.success().put("nick", user.getNick());
    }

    public ZenResult test() {
        return ZenResult.success().setData("test");
    }

    public ZenResult test2() {
        return ZenResult.success().setData("test2");
    }

    public ZenResult test3() {
        return ZenResult.success().setData("test3");
    }
}
